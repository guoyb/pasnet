import numpy as np
import random as rn
# fix random seeds to reduce variability of results
np.random.seed(2019)
rn.seed(2019)
import tensorflow as tf
from keras import backend as K
tf.set_random_seed(2019)
import sys
import os
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import np_utils
from keras import initializers
from sklearn.metrics import classification_report, confusion_matrix, precision_score, recall_score, f1_score, cohen_kappa_score, precision_recall_curve, average_precision_score, roc_auc_score, matthews_corrcoef
from sklearn.model_selection import train_test_split
import scipy.io as spio
import os.path
import Datatransform DataRep
from utils import *
from plot import *
import matplotlib.pyplot as plt
from kerasact import *
# --------------- Arguments Parser --------------
import argparse
parser = argparse.ArgumentParser(description='Parameters to be used for PASNet.')
parser.add_argument('--PosInputFile', help='Positive input file', required=True)
parser.add_argument('--NegInputFile', help='Negative input file', required=True)
parser.add_argument('--DataName', help='Data file name', required=True)
parser.add_argument('--FileName', help='File names tag', required=True)
parser.add_argument('-kw', dest='kw', type=int, default=5, help='length of word in DNA sequences')
parser.add_argument('-n', dest='n', type=int, default=90, help='dimension of word')
parser.add_argument('-name', dest='name', type=str, default='human_AATAAA')
parser.add_argument('-init', dest='init', action='store_true', default=True, help='initialze embedding vector')
parser.add_argument('-trainable', dest='trainable', action='store_true', default=True, help='embedding vectors trainable')
parser.add_argument('-sequence_length', dest='sequence_length', type=int, default=600, help='')
args = parser.parse_args()
#--------------- Define some parameters --------------
nb_classes=2
nb_epoch = 200

#------------ load data -------------

def load_data():    
    [_, _, _, _, data, labels]=DataRep.Data2Text(args.PosInputFile, args.NegInputFile, args.kw, args.DataName)
    
    labels=labels[:,0]
    tokenizer = Tokenizer(lower=False)
    tokenizer.fit_on_texts(data)
    sequences = tokenizer.texts_to_sequences(data)
    
    
    word_index = tokenizer.word_index
    print ('args.sequence_length',args.sequence_length)
    data = pad_sequences(sequences, maxlen = args.sequence_length)
    print ('Found %s unique tokens.' % len(word_index))
    print ('Spliting train, valid, test parts...')  
    # mixing the data
    indices = np.arange(data.shape[0])
    np.random.shuffle(indices)
    print (indices[:10],data.shape)
    X = data[indices]
    Y = labels[indices]
    print (np.shape(X))
    print('data original shapes: '+str(X.shape)+'\n')
    Y = np_utils.to_categorical(Y, nb_classes)
    return X,Y,word_index
    
#-------------Input data preparation for GCHN-------------   
X,Y,word_index = load_data()
n_seqs = X.shape[0]

n_tr1 = int(n_seqs * 0.75)
n_va = int(n_tr1 * 0.20)
n_tr = n_tr1-n_va
n_te = n_seqs - n_tr1

indices1 = np.arange(n_seqs)
np.random.shuffle(indices1)
print (indices1[:10])
X = X[indices1]
Y = Y[indices1]
print('n_seqs\n %s n_tr \n %s n_va \n %s n_te \n %s'% (len(X),n_tr,n_va,n_te))
X_train1 = X[:n_tr1]
Y_train1 = Y[:n_tr1]
X_train = X_train1[:n_tr]
Y_train = Y_train1[:n_tr]
X_valid = X_train1[-n_va:]
Y_valid = Y_train1[-n_va:]

X_test = X[-n_te:]
Y_test = Y[-n_te:] 

test_label_rep = Y_test

print('training samples: ', X_train.shape[0])
print('valid samples: ', X_valid.shape[0])
print('testing samples: ', X_test.shape[0])

nb_words = len(word_index)
if args.init:
        print ('initialize embedding layer with word vectors','./Data/%s_%dword_%dvectors.txt' % (args.name, args.kw, args.n))
        kmer2vec={}
        f = open('./Data/%s_%dword_%dvectors.txt' % (args.name, args.kw, args.n))
        for line in f:
            values = line.split()
            try:
                kmer = values[0]
                coefs = np.asarray(values[1:], dtype='float32')
                kmer2vec[kmer] = coefs
            except:pass
        f.close()
        embedding_matrix = np.zeros((nb_words+1, args.n))
        for kmer, i in word_index.items():
            if i > (nb_words):
                continue
            vector = kmer2vec.get(kmer)
            # print (vector)
            if vector is not None:
                embedding_matrix[i] = vector


#--------------- Creating the model --------------

print("\nCreating the model ...\n")
for dropout_rate in [0.1]:
    FileNameR = 'Results/dropout_rate%s_%s_Results.txt' %(dropout_rate,args.FileName)
    Result_file=open(FileNameR,'w')
    lr = 0.000003
    batch_size = 64
    for filters in [32, 64, 128]:
        for  ks in [21,27,33,39,45,51]:
                    for units in [32, 64, 128]:
                        print ('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',ks)
                        history = LossHistory()
                        model = build_PASNet(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix,lr, ks, units)
                        # Fit the model                
                        filepath="Models/%s_bestModel(dropout_rate%s_%sfilterslr%sks%sunits%s).h5" %(args.FileName, dropout_rate, filters, lr, ks, units)
                        checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True)
                        early_stopping = EarlyStopping(monitor='val_loss', patience=10, verbose=1, mode='auto')
                        hist= model.fit(X_train, Y_train, epochs=nb_epoch, batch_size=batch_size, verbose=2, validation_data=(X_valid, Y_valid), callbacks=[history, early_stopping, checkpoint])
                        print('Plotting the acc-loss curve...')
                        fname= "%s_bestModel(dropout_rate%s_%sfilterslr%sks%sunits%s).h5" %(args.FileName, dropout_rate, filters, lr, ks, units)
                        history.loss_plot('epoch',fname)
                        model.load_weights("Models/%s_bestModel(dropout_rate%s_%sfilterslr%sks%sunits%s).h5" %( args.FileName,dropout_rate, filters, lr, ks, units))
                        # evaluate the model  
                        # activations = get_activations(model,X_train)
                        # display_heatmaps(activations, X_train, save=True)
                        # exit()
                        scores = model.evaluate(X_test, Y_test, verbose=1)
                        print ('>>>>>>>',scores)   
                        # scoreTr = model.evaluate(X_train, Y_train, verbose=1)
                        # pred_data = model.predict_classes(X_test)
                        pred_data1 = model.predict(X_test)
                        pred_data = np.argmax(pred_data1,axis=1)     
              
                        #------------- Results -------------            
                        cm1 = confusion_matrix(np.argmax(test_label_rep,axis=1), pred_data)
                        average_precision = average_precision_score(np.argmax(test_label_rep,axis=1), pred_data)
                        precision, recall, _ = precision_recall_curve(np.argmax(test_label_rep,axis=1), pred_data)
                        total1=sum(sum(cm1))
                        accuracy1=(cm1[0,0]+cm1[1,1])/total1
                        sensitivity1 = cm1[0,0]/(cm1[0,0]+cm1[0,1])
                        specificity1 = cm1[1,1]/(cm1[1,0]+cm1[1,1])            
                        print('Confusion Matrix : \n', cm1)
                        print ('Accuracy : ', accuracy1)
                        print('Sensitivity : ', sensitivity1)
                        print('Specificity : ', specificity1)
                        print ('AUC: '+str(roc_auc_score(np.argmax(test_label_rep,axis=1), pred_data)))
                        print ('AUPR: '+str(average_precision))
                        print ('Clasification report:\n', classification_report(np.argmax(test_label_rep, axis=1), pred_data))
                        print ('\nprecision score:',precision_score(np.argmax(test_label_rep, axis=1), pred_data), '\n')
                        print ('\nrecall score:',recall_score(np.argmax(test_label_rep, axis=1), pred_data), '\n')
                        print ('\nf1 score:',f1_score(np.argmax(test_label_rep, axis=1), pred_data), '\n')
                        print ('\ncohen kappa score:',cohen_kappa_score(np.argmax(test_label_rep, axis=1), pred_data), '\n')
                        print("MCC: %f "%matthews_corrcoef(np.argmax(test_label_rep, axis=1),pred_data))
                        print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100)) 
                        model_id = "%s_bestModel_batchsize%s(dropout_rate%s_%sfilterslr%sks%sunits%s).h5" %(args.FileName, batch_size,dropout_rate,  filters, lr, ks, units)
                        Result_file.write('model id: ' + str(model_id)+'\n')
                        Result_file.write('Accuracy : '+ str(accuracy1) +'\n')
                        Result_file.write("%s: %.2f%%\n" % (model.metrics_names[1], scores[1]*100))
                        Result_file.write("Sensitivity: %.2f%%\n" %( sensitivity1*100))
                        Result_file.write("Specificity: %.2f%%\n" %(specificity1*100))
                        Result_file.write('AUPR: '+str(average_precision)+'\n')
                        Result_file.write('AUC: '+str(roc_auc_score(np.argmax(test_label_rep,axis=1), pred_data))+'\n')
                        ps = precision_score(np.argmax(test_label_rep,axis=1), pred_data)
                        Result_file.write('precision:'+ str(ps)+'\n' )
                        rs = recall_score(np.argmax(test_label_rep,axis=1), pred_data)
                        Result_file.write('recall:'+ str(rs)+'\n' )
                        f1 = f1_score(np.argmax(test_label_rep,axis=1), pred_data)
                        Result_file.write('f1:'+ str(f1)+'\n' )
                        Result_file.write('\n---------------------------------------------------------------------------\n')
                        K.clear_session()
    Result_file.close()
